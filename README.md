# LUAS Android Demo
Example multi-module Android App written in Kotlin using clean architecture, coroutines & MVVM.

### The 4 layers are:
- **data**: *includes API, repository implementations, model, mappers and date utility classes*
- **domain**: *includes usecases and interfaces for the mapper and repository*
- **presentation**: *includes ViewModels*
- **ui**: *includes activities and resources*

------------------------------

### Testing
#### The project includes unit tests for the data, domain and presentation layers using:
- Mockito Kotlin
- Mock Web server (Data layer only)

Note that there is a module named **mock**. The mock module is used by the other modules for mocking return values when using mockito in unit tests.


# Requirements

Our employees commute from city center to the office using LUAS every
day. To avoid waiting too much at the stops, people would like to have an app where it is
possible to see trams forecast. So, to help our employee, write a simple app considering
only the following requests:

- *Given I am a LUAS passenger
When I open the app from 00:00 – 12:00
Then I should see trams forecast from Marlborough LUAS stop towards Outbound*

- *Given I am a LUAS passenger
When I open the app from 12:01 – 23:59
Then I should see trams forecast from Stillorgan LUAS stop towards Inbound*

- *Given I am on the stop forecast info screen
When I tap on the refresh button
Then the forecast data should be updated*
